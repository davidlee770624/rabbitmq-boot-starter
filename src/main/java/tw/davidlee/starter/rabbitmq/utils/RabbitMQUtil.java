package tw.davidlee.starter.rabbitmq.utils;

import tw.davidlee.starter.rabbitmq.enums.RabbitMQEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Date;

@Slf4j
@Component
public class RabbitMQUtil {

    @Resource
    private RabbitTemplate rabbitTemplate;

    public void sendMessage(RabbitMQEnum mqQueue, Object object) {
        log.info("rabbitmq sendMessage Exchange={} time={}",mqQueue, new Date());
        rabbitTemplate.convertAndSend(mqQueue.getExchange(), mqQueue.getRoutingKey(), object);
    }

    public void delayMessage(RabbitMQEnum mqQueue, long time, Object object) {
        log.info("rabbitmq delayMessage Exchange={} time={}",mqQueue, new Date());
        rabbitTemplate.convertAndSend(mqQueue.getExchange(), mqQueue.getRoutingKey(), object, message -> {
            message.getMessageProperties().setHeader("x-delay", time);
            return message;
        });
    }

    public void dlxDelayMessage(RabbitMQEnum mqQueue, long time, Object object) {
        log.info("rabbitmq dlxDelayMessage Exchange={} time={}",mqQueue, new Date());
        rabbitTemplate.convertAndSend(mqQueue.getExchange(), mqQueue.getRoutingKey(), object, message -> {
            message.getMessageProperties().setExpiration(time + "");
            return message;
        });
    }

    public void sendAndReceive(RabbitMQEnum mqQueue, Object object) {
        log.info("rabbitmq sendAndReceive Exchange={} time={}",mqQueue, new Date());
        rabbitTemplate.convertSendAndReceive(mqQueue.getExchange(), mqQueue.getRoutingKey(), object);
    }
}
