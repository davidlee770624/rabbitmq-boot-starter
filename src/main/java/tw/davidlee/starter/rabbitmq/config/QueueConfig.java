package tw.davidlee.starter.rabbitmq.config;

import tw.davidlee.starter.rabbitmq.constant.QueueNameConstant;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 * 建立消息對列 Queue
 */
@Configuration
@Component
public class QueueConfig {

    /**
     * 建立 Queue
     */
    @Bean
    public Queue consumerMessageQueue() {
        return new Queue(QueueNameConstant.NOTICE_MSG, true);
    }

}
