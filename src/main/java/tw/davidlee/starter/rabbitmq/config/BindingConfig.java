package tw.davidlee.starter.rabbitmq.config;

import tw.davidlee.starter.rabbitmq.enums.RabbitMQEnum;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;

/**
 *  exchange與queue綁定
 */
@Configuration
public class BindingConfig {

    @Resource
    ExchangeConfig exchangeConfig;

    @Resource
    QueueConfig queueConfig;

    /**
     *  服務通訊
     */
    @Bean
    public Binding bindMQMessage() {
        return BindingBuilder
                .bind(queueConfig.consumerMessageQueue())
                .to(exchangeConfig.messageExchange())
                .with(RabbitMQEnum.NOTICE.getRoutingKey());
    }

}
