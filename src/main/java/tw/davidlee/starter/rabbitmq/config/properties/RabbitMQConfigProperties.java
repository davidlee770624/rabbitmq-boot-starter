package tw.davidlee.starter.rabbitmq.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "tw.davidlee.rabbitmq" )
@Data
public class RabbitMQConfigProperties {

    private String addresses;
    private String username;
    private String password;
    private Boolean publisherReturns;
    /**
     * 第一次與第二次嘗試傳遞消息之間的間隔。
     * **/
    private Long initialInterval;
    /**
     * 兩次嘗試之間的最大間隔。
     * **/
    private Long maxInterval;
    /**
     * 適用於上一個傳遞重試間隔的乘數。
     * **/
    private Double multiplier;
    /**
     * 連線超時
     * **/
    private Integer connectionTimeout;
    /**
     * 核心线程数
     * **/
    private Integer taskExecutorCorePoolSize;
    /**
     * 最大线程数
     * **/
    private Integer taskExecutorMaxPoolSize;
    /**
     * 快取佇列
     * **/
    private Integer taskExecutorQueueCapacity;
    /**
     * 線程池維護線程所允許的空閒時間
     * **/
    private Integer taskExecutorKeepAliveSeconds;
}
