package tw.davidlee.starter.rabbitmq.config;

import tw.davidlee.starter.rabbitmq.constant.ExchangeNameConstant;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 * 建立交換機 Exchange
 */
@Configuration
@Component
public class ExchangeConfig {


    /**
     * 建立型別：direct交換機
     */
    @Bean
    public DirectExchange messageExchange() {
        return new DirectExchange(ExchangeNameConstant.NOTICE_MSG , true, false);
    }

}
