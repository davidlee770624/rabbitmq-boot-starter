package tw.davidlee.starter.rabbitmq.config;

import lombok.extern.slf4j.Slf4j;
import org.aopalliance.aop.Advice;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.rabbit.config.RetryInterceptorBuilder;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.retry.RejectAndDontRequeueRecoverer;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.amqp.RabbitAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.retry.backoff.ExponentialBackOffPolicy;
import org.springframework.retry.interceptor.RetryOperationsInterceptor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import tw.davidlee.starter.rabbitmq.config.properties.RabbitMQConfigProperties;

import java.util.concurrent.ThreadPoolExecutor;


@Slf4j
@Configuration
@EnableRabbit
@EnableConfigurationProperties(RabbitMQConfigProperties.class)
@ComponentScan(value="tw.davidlee.starter.rabbitmq.utils")
@AutoConfigureBefore(RabbitAutoConfiguration.class)
public class RabbitMQConfig {

    @Autowired
    private RabbitMQConfigProperties rabbitMQConfigProperties;

    @Bean
    public ConnectionFactory connectionFactory() {
        CachingConnectionFactory conf = new CachingConnectionFactory();
        conf.setAddresses(rabbitMQConfigProperties.getAddresses());
        conf.setUsername(rabbitMQConfigProperties.getUsername());
        conf.setPassword(rabbitMQConfigProperties.getPassword());
        if(rabbitMQConfigProperties.getPublisherReturns() !=null){
            conf.setPublisherReturns(rabbitMQConfigProperties.getPublisherReturns());
        }
        if(rabbitMQConfigProperties.getConnectionTimeout() !=null){
            conf.setConnectionTimeout(rabbitMQConfigProperties.getConnectionTimeout());
        }
        return conf;
    }

    @Bean
    public RabbitTemplate rabbitTemplate( ConnectionFactory connectionFactory ) {
        RabbitTemplate rb = new RabbitTemplate();
        rb.setConnectionFactory(connectionFactory);
        rb.setMessageConverter(new Jackson2JsonMessageConverter());
        return rb;
    }

    @Bean
    public SimpleRabbitListenerContainerFactory rabbitListenerContainerFactory( ConnectionFactory connectionFactory) {
        SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
        factory.setConnectionFactory(connectionFactory);
        factory.setMessageConverter(new Jackson2JsonMessageConverter());
        if(rabbitMQConfigProperties.getInitialInterval() !=null && rabbitMQConfigProperties.getMultiplier() !=null && rabbitMQConfigProperties.getMaxInterval() !=null){
            factory.setAdviceChain(new Advice[]{interceptor()});
        }
        factory.setTaskExecutor(taskExecutor());
        return factory;
    }

    @Bean
    public RabbitAdmin rabbitAdmin( ConnectionFactory connectionFactory){
        RabbitAdmin rabbitAdmin = new RabbitAdmin(connectionFactory);
        return rabbitAdmin;
    }

    private TaskExecutor taskExecutor () {
        ThreadPoolTaskExecutor task = new ThreadPoolTaskExecutor();
        if(rabbitMQConfigProperties.getTaskExecutorCorePoolSize() !=null ){
            task.setCorePoolSize(rabbitMQConfigProperties.getTaskExecutorCorePoolSize());
        }
        if(rabbitMQConfigProperties.getTaskExecutorMaxPoolSize() !=null ){
            task.setMaxPoolSize(rabbitMQConfigProperties.getTaskExecutorMaxPoolSize());
        }
        if(rabbitMQConfigProperties.getTaskExecutorQueueCapacity() !=null ){
            task.setQueueCapacity(rabbitMQConfigProperties.getTaskExecutorQueueCapacity());
        }
        if(rabbitMQConfigProperties.getTaskExecutorKeepAliveSeconds() !=null ){
            task.setKeepAliveSeconds(rabbitMQConfigProperties.getTaskExecutorKeepAliveSeconds());
        }
        task.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy()); //對拒絕task的處理策略
        task.setThreadNamePrefix("MSG-");
        task.initialize();
        return task;
    }


    private RetryOperationsInterceptor interceptor() {
        ExponentialBackOffPolicy backOffPolicy = new ExponentialBackOffPolicy();
        if(rabbitMQConfigProperties.getInitialInterval() != null){
            backOffPolicy.setInitialInterval(rabbitMQConfigProperties.getInitialInterval());
        }
        if(rabbitMQConfigProperties.getMultiplier() != null){
            backOffPolicy.setMultiplier(rabbitMQConfigProperties.getMultiplier());
        }
        if(rabbitMQConfigProperties.getMaxInterval() != null){
            backOffPolicy.setMaxInterval(rabbitMQConfigProperties.getMaxInterval());
        }
        return RetryInterceptorBuilder.stateless()
                .backOffPolicy(backOffPolicy)
                .recoverer(new RejectAndDontRequeueRecoverer())
                .build();
    }


}
