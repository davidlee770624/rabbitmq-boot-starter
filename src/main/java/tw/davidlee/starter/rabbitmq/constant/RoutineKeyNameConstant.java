package tw.davidlee.starter.rabbitmq.constant;

public interface RoutineKeyNameConstant {

    /**
     *  PRODUCER
     */
    String PRODUCER = "producer";

}
