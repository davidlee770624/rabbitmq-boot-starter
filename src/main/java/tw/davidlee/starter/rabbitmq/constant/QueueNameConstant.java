package tw.davidlee.starter.rabbitmq.constant;

public interface QueueNameConstant {

    /**
     *  NOTICE_MSG
     */
    String NOTICE_MSG = "notice_msg";

}
