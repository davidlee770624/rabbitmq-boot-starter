//package com.sc.rabbitmq.handler;
//
//import lombok.extern.slf4j.Slf4j;
//import org.apache.commons.lang3.exception.ExceptionUtils;
//import org.springframework.amqp.core.Message;
//import org.springframework.amqp.rabbit.listener.api.RabbitListenerErrorHandler;
//import org.springframework.amqp.rabbit.support.ListenerExecutionFailedException;
//import org.springframework.stereotype.Component;
//
//@Slf4j
//@Component
//public class RabbitExceptionHandler implements RabbitListenerErrorHandler {
//
//    @Override
//    public Object handleError(Message amqpMessage, org.springframework.messaging.Message<?> message, ListenerExecutionFailedException exception) throws Exception {
//        log.error(ExceptionUtils.getStackTrace(exception));
//        throw exception;
//    }
//}
