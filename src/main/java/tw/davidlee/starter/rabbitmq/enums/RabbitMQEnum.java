package tw.davidlee.starter.rabbitmq.enums;


import tw.davidlee.starter.rabbitmq.constant.ExchangeNameConstant;
import tw.davidlee.starter.rabbitmq.constant.QueueNameConstant;
import tw.davidlee.starter.rabbitmq.constant.RoutineKeyNameConstant;
import lombok.Getter;

/**
 *  定義 exchange queue routinekey 關係
 */
public enum RabbitMQEnum {

    NOTICE(ExchangeNameConstant.NOTICE_MSG , QueueNameConstant.NOTICE_MSG , RoutineKeyNameConstant.PRODUCER),;

    @Getter
    String exchange;

    @Getter
    String queueName;

    @Getter
    String routingKey;

    RabbitMQEnum(String exchange , String queueName, String routingKey) {
        this.exchange = exchange;
        this.queueName = queueName;
        this.routingKey = routingKey;
    }
}
